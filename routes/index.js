import express from "express";
import usersController from "../controllers/usersController"

const router = express.Router();

router.get('/api/v1/users', usersController.getAllUsers);
router.get('/api/v1/users/:id', usersController.getUser);
router.post('/api/v1/users', usersController.createUser);

export default router;
