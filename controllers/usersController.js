import models from "../models";

class UsersController {
  getAllUsers(req, res) {
    models.User.findAll().then(users =>
      res.status(200).send({
        success: "true",
        message: "users retrieved successfully",
        users: users
      })
    );
  }

  getUser(req, res) {
    models.User.findOne({ where: { id: req.params.id } }).then(user => {
      if (user) {
        return res.status(200).send({
          success: "true",
          user: user
        });
      } else {
        return res
          .status(404)
          .send({ success: "false", message: "user does not exist" });
      }
    });
  }

  createUser(req, res) {
    if (!req.body.name) {
      return res.status(400).send({
        success: "false",
        message: "name is required"
      });
    } else if (!req.body.email) {
      return res.status(400).send({
        success: "false",
        message: "email is required"
      });
    }
    const user = {
      name: req.body.name,
      email: req.body.email
    };
    models.User.create(user)
      .then(user => {
        return res.status(201).send({
          success: "true",
          message: "user added successfully",
          user
        });
      })
      .catch(function(err) {
        // print the error details
        console.log(err, request.body.email);
      });
  }
}

const usersController = new UsersController();
export default usersController;
