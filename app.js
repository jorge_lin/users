import express from "express";
import router from './routes/index.js';
import bodyParser from "body-parser";

require('dotenv').config()
const app = express(); // get all users
const PORT = process.env.PORT;

// Parse incoming requests data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(router);

app.listen(PORT, () => {
  console.log(`server running on port ${PORT}`);
});


// Static files
app.use(express.static('public'))

// Backend Proxy
var proxy = require('http-proxy-middleware');
var apiProxy = proxy('/V2', { target: 'https://services.odata.org', changeOrigin: true });
app.use(apiProxy)