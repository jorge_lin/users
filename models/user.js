'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    name: DataTypes.STRING,
    email: DataTypes.STRING,
  }, {allowNull: false});
  User.associate = (models) => {
    User.hasMany(models.Location, {
      foreignKey: 'userId'
    })
  };
  return User;
};