'use strict';
module.exports = (sequelize, DataTypes) => {
  const Location = sequelize.define('Location', {
    name: DataTypes.STRING
  }, {});
  Location.associate = function(models) {
    // associations can be defined here
    Location.belongsTo(models.User, {
      foreignKey:'userId',
      onDelete:'CASCADE'
    })
  };
  return Location;
};